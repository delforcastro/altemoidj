from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
# Vistas de la aplicación 'terminal'
from terminal.models import Lvidaqa, Lasom, Lmala, Nmen
from terminal.forms import LvidaqaForm, LasomForm, LmalaForm, NmenForm

#Vistas para CRUD Terminal


def Tablon(request):
	terminales = Lvidaqa.objects.all()
	puertas = Lasom.objects.all()
	plataformas = Lmala.objects.all()
	boleterias = Nmen.objects.all()

	contexto = {'terminales': terminales,'puertas': puertas, 'plataformas':plataformas, 'boleterias': boleterias}

	return render(request, 'tablon/tablon.html', contexto)


class NuevaTerminal (CreateView):
	model = Lvidaqa
	form_class = LvidaqaForm
	template_name = 'lvidaqa/iotlvidaqa.html'
	success_url = reverse_lazy('terminal:lista_lvidaqa')

class ListaTerminal (ListView):
	model = Lvidaqa
	template_name = 'lvidaqa/nachaxanlvidaqa.html'

class EditarTerminal (UpdateView):
	model = Lvidaqa
	form_class = LvidaqaForm
	template_name = 'lvidaqa/iotlvidaqa.html'
	success_url = reverse_lazy('terminal:lista_lvidaqa')

class EliminarTerminal (DeleteView):
	model = Lvidaqa
	template_name = 'lvidaqa/aqtaclvidaqa.html'
	success_url = reverse_lazy('terminal:tablon')



#Vistas para CRUD Puerta
class NuevaPuerta (CreateView):
	model = Lasom
	form_class = LasomForm
	template_name = 'lvidaqa/iot/lasom.html'
	success_url = reverse_lazy('terminal:tablon')

class ListaPuerta (ListView):
	model = Lasom
	template_name = 'lvidaqa/nachaxanlasom.html'

class EditarPuerta (UpdateView):
	model = Lasom
	form_class = LasomForm
	template_name = 'lvidaqa/iot/lasom.html'
	success_url = reverse_lazy('terminal:tablon')

class EliminarPuerta (DeleteView):
	model = Lasom
	template_name = 'lvidaqa/aqtaclasom.html'
	success_url = reverse_lazy('terminal:tablon')

#Vistas para CRUD Plataforma
class NuevaPlataforma (CreateView):
	model = Lmala
	form_class = LmalaForm
	template_name = 'lvidaqa/iot/lmala.html'
	success_url = reverse_lazy('terminal:tablon')

class ListaPlataforma (ListView):
	model = Lmala
	template_name = 'lvidaqa/nachaxanlmala.html'


class EditarPlataforma (UpdateView):
	model = Lmala
	form_class = LmalaForm
	template_name = 'lvidaqa/iot/lmala.html'
	success_url = reverse_lazy('terminal:tablon')

class EliminarPlataforma (DeleteView):
	model = Lmala
	template_name = 'lvidaqa/aqtaclmala.html'
	success_url = reverse_lazy('terminal:tablon')

#Vistas para CRUD Boleteria
class NuevaBoleteria (CreateView):
	model = Nmen
	form_class = NmenForm
	template_name = 'lvidaqa/iot/nmen.html'
	success_url = reverse_lazy('terminal:tablon')

class ListaBoleteria (ListView):
	model = Nmen
	template_name = 'lvidaqa/nachaxannmen.html'

class EditarBoleteria(UpdateView):
	model = Nmen
	form_class = NmenForm
	template_name = 'lvidaqa/iot/nmen.html'
	success_url = reverse_lazy('terminal:tablon')

class ElimininarBoleteria (DeleteView):
	model = Nmen
	template_name = 'lvidaqa/aqtacnmen.html'
	success_url = reverse_lazy('terminal:tablon')
