from django import forms

from terminal.models import Lvidaqa, Lasom, Lmala, Nmen
class LvidaqaForm (forms.ModelForm):
    class Meta:
        model = Lvidaqa

        fields = [
            'denominacion',
            'domicilio',
            'telefono',
            'email',
            'nro_impositivo',
        ]

        labels = {
            'denominacion':'Denominación',
            'domicilio' : 'Domicilio',
            'telefono':'Teléfono',
            'email':'Correo electrónico',
            'nro_impositivo':'Identificación tributaria',
         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'domicilio': forms.TextInput(attrs={'class':'form-control'}),
             'telefono' : forms.TextInput(attrs={'class':'form-control'}),
             'email' : forms.EmailInput(attrs={'class':'form-control'}),
             'nro_impositivo' : forms.TextInput(attrs={'class':'form-control'}),
         }

class LasomForm (forms.ModelForm):
    class Meta:
        model = Lasom

        fields = [
            'terminal',
            'denominacion',
        ]

        labels = {
            'terminal':'Estación terminal',
            'denominacion':'Denominación o código',
        }

        widgets = {
            'terminal':forms.Select(attrs={'class':'form-control'}),
            'denominacion':forms.TextInput(attrs={'class':'form-control'}),

        }

class LmalaForm (forms.ModelForm):
    class Meta:
        model = Lmala

        fields = [
            'terminal',
            'puertaembarque',
            'numeroID',
            'denominacion',
            'rutaAudio',

        ]

        help_texts = {
        'denominacion':'Coloque el número de plataforma aquí',
        }

        labels = {
            'terminal':'Estación terminal',
            'puertaembarque':'Puerta de embarque',
            'numeroID':'Número ID',
            'denominacion':'Denominación',
            'rutaAudio':'Ruta de archivo de audio',

        }

        widgets = {
            'terminal':forms.Select(attrs={'class':'form-control'}),
            'puertaembarque':forms.Select(attrs={'class':'form-control'}),
            'numeroID':forms.TextInput(attrs={'class':'form-control'}),
            'denominacion':forms.TextInput(attrs={'class':'form-control'}),
            'rutaAudio':forms.TextInput(attrs={'class':'form-control'}),

        }

class NmenForm (forms.ModelForm):
    class Meta:
        model = Nmen

        fields = [
            'terminal',
            'numeroID',
            'telefono',
            'internoTelefono',
            'email',
            'nombreContacto',
            'telefonoEmergencia',
            'status',

        ]

        labels = {
            'terminal':'Estación terminal',
            'numeroID':'Número ID',
            'telefono':'Teléfono público',
            'internoTelefono':'Número interno',
            'email':'Correo electrónico',
            'nombreContacto':'Apellido y nombre responsable',
            'telefonoEmergencia':'Contacto de responsable',
            'status':'Estado de la boletería',


        }
        widgets = {
            'terminal':forms.Select(attrs={'class':'form-control'}),
            'numeroID':forms.TextInput(attrs={'class':'form-control'}),
            'telefono':forms.TextInput(attrs={'class':'form-control'}),
            'internoTelefono':forms.TextInput(attrs={'class':'form-control'}),
            'email':forms.EmailInput(attrs={'class':'form-control'}),
            'nombreContacto':forms.TextInput(attrs={'class':'form-control'}),
            'telefonoEmergencia':forms.TextInput(attrs={'class':'form-control'}),
            'status':forms.Select(attrs={'class':'form-control'}),

        }
