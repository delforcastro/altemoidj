from django.urls import include, path, re_path
from django.contrib.auth.decorators import login_required

from . import views
app_name = "terminal"

urlpatterns = [
                path('tablon', login_required(views.Tablon), name='tablon'),

                path('nuevaterminal', login_required(views.NuevaTerminal.as_view()), name='nueva_lvidaqa'),
                path('listaterminal', login_required(views.ListaTerminal.as_view()), name='lista_lvidaqa'),
                re_path(r'^editarterminal/(?P<pk>\d+)/$', login_required(views.EditarTerminal.as_view()), name='editar_lvidaqa'),
                re_path(r'^eliminarterminal/(?P<pk>\d+)/$', login_required(views.EliminarTerminal.as_view()), name='eliminar_lvidaqa'),


                path('nuevapuerta', login_required(views.NuevaPuerta.as_view()), name='nueva_lasom'),
                path('listapuerta', login_required(views.ListaPuerta.as_view()), name='lista_lasom'),
                re_path(r'^editarpuerta/(?P<pk>\d+)/$', login_required(views.EditarPuerta.as_view()), name='editar_lasom'),
                re_path(r'^eliminarpuerta/(?P<pk>\d+)/$', login_required(views.EliminarPuerta.as_view()), name='eliminar_lasom'),


                path('nuevaplataforma', login_required(views.NuevaPlataforma.as_view()), name='nueva_lmala'),
                path('listaplataforma', login_required(views.ListaPlataforma.as_view()), name='lista_lmala'),
                re_path(r'^editarplataforma/(?P<pk>\d+)/$', login_required(views.EditarPlataforma.as_view()), name='editar_lmala'),
                re_path(r'^eliminarplataforma/(?P<pk>\d+)/$', login_required(views.EliminarPlataforma.as_view()), name='eliminar_lmala'),


                path('nuevaboleteria', login_required(views.NuevaBoleteria.as_view()), name='nueva_nmen'),
                path('listaboleteria', login_required(views.ListaBoleteria.as_view()), name='lista_nmen'),
                re_path(r'^editarboleteria/(?P<pk>\d+)/$', login_required(views.EditarBoleteria.as_view()), name='editar_nmen'),
                re_path(r'^eliminarboleteria/(?P<pk>\d+)/$', login_required(views.ElimininarBoleteria.as_view()), name='eliminar_nmen'), #falta implementar esto




]
