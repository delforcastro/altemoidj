from django.db import models

#Los modelos de la aplicación Terminal están pensados
#para la administración de la estructura  física propia de la Terminal
#incluye datos generales de la estación terminal, plataformas, boleterías

class Status (models.Model):
    denominacion = models.CharField(max_length=150)

    def __str__(self):
        return self.denominacion


class Lvidaqa (models.Model):
    #'Lvidaqa' significa 'Terminal'
    #'Lvidaqa' means 'Station'
    #Esta clase representa la Terminal
    denominacion = models.CharField(max_length=150)
    domicilio = models.CharField(max_length=150, null=True, blank=True)
    telefono = models.CharField(max_length=40, blank=True)
    email = models.EmailField(max_length=150)
    #nro_impositivo está pensado para almacenar un número
    #de identificación tributaria, como CUIT en Argentina.
    nro_impositivo = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.denominacion

class Lasom (models.Model):
    #Lasom significa 'Puerta'
    #Lasom means 'Door'
    #Esta clase representa las Puertas de embarque

    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)
    #Datos de la puerta de embarque:
    denominacion = models.CharField(max_length=150)
    def __str__(self):
        return self.denominacion

class Lmala (models.Model):
    #Lmala' significa 'Plataforma'
    #Lmala' means 'Platform'
    #Esta clase representa las plataformas físicas de la terminal

    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)
    #Datos de la plataforma:
    numeroID = models.IntegerField()
    denominacion = models.CharField(max_length=30)
    rutaAudio = models.CharField(max_length=150, null=True, blank=True)
    puertaembarque = models.ForeignKey(Lasom, on_delete=models.CASCADE)
    def __str__(self):
        return self.denominacion


class Iqueuoxon (models.Model):
    #Iqueuoxon significa 'lo lleva de viaje'
    #Iqueuoxon means ''
    #Esta clase representa a las empresas de transporte

    #FK para asociar a terminal
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)

    #Datos de la empresa de transporte
    razonSocial = models.CharField(max_length=150, null=False, blank=False)
    nombreFantasia = models.CharField(max_length=150, null=False, blank=False)

    #nro_impositivo está pensado para almacenar un número
    #de identificación tributaria, como CUIT en Argentina.
    nro_impositivo = models.CharField(max_length=20, blank=True)
    domicilioFiscal = models.CharField(max_length=150, blank=True)
    telefono = models.CharField(max_length=40, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    sitioWeb = models.CharField(max_length=100, null=True, blank=True)
    

    def __str__(self):
        return self.razonSocial

class Nmen (models.Model):
    #Nmen significa '[él] Vende'
    #Nmen means '[he] Sells'
    #Esta clase representa las Boleterías de la terminal

    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)
    #Datos de la boletería
    numeroID = models.IntegerField()
    telefono = models.CharField(max_length=50, null=True, blank=True)
    internoTelefono = models.CharField(max_length=10, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    nombreContacto = models.CharField(max_length=150)
    telefonoEmergencia = models.CharField(max_length=50, null=True, blank=True)
    flag = models.IntegerField(null=True, blank=True)

    #En esta relación muchos a muchos establecemos la asociación entre
    #boleterías y empresas de transporte (muchos a muchos)
    empresas = models.ManyToManyField(Iqueuoxon)

    #Estado puede ser: 1 - Activa, normal, 2: Activa, con observacion
    #3: Inactiva, temporalmente, 4: Inactiva indeterminadamente

    status = models.ForeignKey(Status, on_delete=models.CASCADE)
