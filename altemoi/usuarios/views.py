from django.shortcuts import render

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.urls import reverse_lazy
from usuarios.forms import RegistroUsuarioForm

class RegistroUsuario (CreateView):
    model = User
    template_name = 'qom/iotqom.html'
    form_class = RegistroUsuarioForm
    success_url = reverse_lazy('terminal:lista_lvidaqa') #Esto hay que cambiarlo
